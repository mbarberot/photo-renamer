import PhotoRenamer from "../../main/ts/rename/PhotoRenamer";
import RenameExecutor from "../../main/ts/rename/executor/RenameExecutor";
import RenameRequest from "../../main/ts/rename/executor/RenameRequest";
import PhotoFinder from "../../main/ts/rename/finder/PhotoFinder";
import InMemorySource from "../../main/ts/rename/finder/source/InMemorySource";

class DummyRenameExecutor implements RenameExecutor {

   initCalled = false
   requests: RenameRequest[] = []

   init(): void {
      this.initCalled = true
   }

   rename(request: RenameRequest): void {
      this.requests.push(request)
   }
}

test("Rename photos", () => {
   // Arrange
   const inMemorySource = new InMemorySource([
      "IMG_20220312_235138.jpg",
      "PXL_20220312_223947.jpg",
      "WP_20131031_00220131102233713.jpg",
      "20150813_143311.jpg",
      "WP_20171205_004.jpg",
      "2022-03-12.jpg",
      "Mon Ange.png"
   ])
   const executor = new DummyRenameExecutor()
   const photoRenamer = new PhotoRenamer(new PhotoFinder(inMemorySource), executor)

   // Act
   photoRenamer.renamePhotos("/fakepath/source", "/fakepath/target")

   // Assert
   expect(executor.initCalled).toBe(true)
   expect(executor.requests).toIncludeSameMembers([
      { from: "IMG_20220312_235138.jpg", to: "2022-03-12 (1).jpg" },
      { from: "PXL_20220312_223947.jpg", to: "2022-03-12 (2).jpg" },
      { from: "WP_20131031_00220131102233713.jpg", to: "WP_20131031_00220131102233713.jpg" },
      { from: "20150813_143311.jpg", to: "2015-08-13.jpg" },
      { from: "WP_20171205_004.jpg", to: "WP_20171205_004.jpg" },
      { from: "2022-03-12.jpg", to: "2022-03-12 (3).jpg" },
      { from: "Mon Ange.png", to: "Mon Ange.png" },
   ] as RenameRequest[])
})