import PhotoRenamer from "../../main/ts/rename/PhotoRenamer"
import PhotoFactory from "../../main/ts/rename/finder/PhotoFactory"

test.each<{ originalFilename: string, expectedFilename: string, testTitle: string }>([
    { originalFilename: "IMG_20220312_235138.jpg", expectedFilename: "2022-03-12.jpg", testTitle: "rename a IMG_yyyyMMdd_######.jpg into yyyy-MM-dd.jpg" },
    { originalFilename: "PXL_20220312_235138.jpg", expectedFilename: "2022-03-12.jpg", testTitle: "rename a PXL_yyyyMMdd_######.jpg into yyyy-MM-dd.jpg" },
    { originalFilename: "PXL_20220312_235138.MP.jpg", expectedFilename: "2022-03-12.jpg", testTitle: "rename a PXL_yyyyMMdd_######.MP.jpg into yyyy-MM-dd.jpg" },
    { originalFilename: "20220312_235138.jpg", expectedFilename: "2022-03-12.jpg", testTitle: "rename a yyyyMMdd_######.jpg into yyyy-MM-dd.jpg" },
    { originalFilename: "Lémurien au zoo.jpg", expectedFilename: "Lémurien au zoo.jpg", testTitle: "do nothing if the pattern is unknown" },
])('$testTitle', ({ originalFilename, expectedFilename }) => {
    // Arrange
    const renamer = new PhotoRenamer()
    const photo = new PhotoFactory().fromFilename(originalFilename)

    // Act
    const result = renamer.rename([photo])

    // Assert
    expect(result).toStrictEqual([{ from: originalFilename, to: expectedFilename }])
})
