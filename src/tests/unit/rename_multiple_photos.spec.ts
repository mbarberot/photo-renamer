import PhotoRenamer from "../../main/ts/rename/PhotoRenamer"
import RenameRequest from "../../main/ts/rename/executor/RenameRequest"
import PhotoFactory from "../../main/ts/rename/finder/PhotoFactory"

test.each<{ input: string[], expected: RenameRequest[], testTitle: string }>([
    {
        testTitle: "Rename multiple files",
        input: ["IMG_20220312_235138.jpg", "PXL_20220313_223947.jpg"],
        expected: [{
            from: "IMG_20220312_235138.jpg",
            to: "2022-03-12.jpg"
        }, {
            from: "PXL_20220313_223947.jpg",
            to: "2022-03-13.jpg"
        }],
    },
    {
        testTitle: "add an increment when multiple file would have the same name",
        input: ["IMG_20220312_235138.jpg", "PXL_20220312_223947.jpg"],
        expected: [{
            from: "IMG_20220312_235138.jpg",
            to: "2022-03-12 (1).jpg"
        }, {
            from: "PXL_20220312_223947.jpg",
            to: "2022-03-12 (2).jpg"
        }],
    },
])("$testTitle", ({ input, expected }) => {
    // Arrange
    const renamer = new PhotoRenamer()
    const factory = new PhotoFactory()
    const photos = input.map(filename => factory.fromFilename(filename))

    // Act
    const result = renamer.rename(photos)

    // Assert
    expect(result).toStrictEqual(expected)
})