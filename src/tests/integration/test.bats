#!/usr/bin/env bash

@test "Happy path" {
    run yarn start src/tests/integration/test_folder/input src/tests/integration/test_folder/output

    echo "$output"
    
    [ "${lines[1]}" = "Photo renamer" ]
    [ "${lines[2]}" = "Source directory : src/tests/integration/test_folder/input" ]
    [ "${lines[3]}" = "Destination directory : src/tests/integration/test_folder/output" ]

    [[ "$output" == *"Dry run mode activated"* ]]

    # PXL Photo  
    [[ "$output" == *"cp PXL_20220316_034033.jpg 2022-03-16.jpg"* ]]

    # IMG Photo
    [[ "$output" == *"cp IMG_20220317_051217.jpg 2022-03-17.jpg"* ]]

    # Photo increment
    [[ "$output" == *"cp 2022-03-15.jpg 2022-03-15 (1).jpg"* ]]
    [[ "$output" == *"cp IMG_20220315_123539.jpg 2022-03-15 (2).jpg"* ]]
    [[ "$output" == *"cp PXL_20220315_110328.jpg 2022-03-15 (3).jpg"* ]]
}
