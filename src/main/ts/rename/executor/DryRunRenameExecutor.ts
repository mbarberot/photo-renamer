import RenameExecutor from "./RenameExecutor";
import RenameRequest from "./RenameRequest";

export default class DryRunRenameExecutor implements RenameExecutor {
    init() {
        console.log("Dry run mode activated")
    }

    rename({from, to}: RenameRequest): void {
        console.log(`cp ${from} ${to}`)
    } 
}