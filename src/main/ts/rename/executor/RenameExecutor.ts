import RenameRequest from "./RenameRequest";

export default interface RenameExecutor {
    init(): void
    rename(request: RenameRequest): void
}