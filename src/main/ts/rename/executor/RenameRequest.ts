type RenameRequest = { from: string, to: string}

export default RenameRequest