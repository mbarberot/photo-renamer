import Photo from "./Photo"

export default abstract class AbstractPhoto implements Photo {

    constructor(
        protected filename: string
    ) {}

    getFilename(): string {
        return this.filename
    }
}