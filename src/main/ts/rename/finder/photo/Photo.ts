export default interface Photo {
    getFilename(): string
}