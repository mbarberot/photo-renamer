import Photo from "./photo/Photo";
import PhotoFactory from "./PhotoFactory";
import FileSystemSource from "./source/FileSystemSource";
import Source from "./source/Source";


export default class PhotoFinder {

    constructor(
        private source: Source = new FileSystemSource(),
        private factory: PhotoFactory = new PhotoFactory(),
    ) { }

    findPhotosIn(path: string): Photo[] {
        return this.source
            .findIn(path)
            .map(filename => this.factory.fromFilename(filename))
    }
}