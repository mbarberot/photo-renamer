import DefaultPhoto from "./photo/DefaultPhoto";
import NotAPhoto from "./photo/NotAPhoto";
import Photo from "./photo/Photo";
import PixelPhoto from "./photo/PixelPhoto";
import StandardPhoto from "./photo/StandardPhoto";

function getExtension(filename: string) {
    const index = filename.lastIndexOf(".")
    return filename.substring(index+1).toLocaleLowerCase()
}

export default class PhotoFactory {

    constructor(
        private allowedExtensions: string[] = ["jpg", "png"]
    ) {}

    fromFilename(filename: string): Photo {    

        if(!this.isAPhoto(filename)) {
            return new NotAPhoto(filename)
        }

        if(filename.startsWith("IMG_")) {
            return new StandardPhoto(filename)
        }

        if(filename.startsWith("PXL_")) {
            return new PixelPhoto(filename)
        }

        return new DefaultPhoto(filename)
    }

    private isAPhoto(filename: string): boolean {
        return this.allowedExtensions.includes(getExtension(filename))
    }
}