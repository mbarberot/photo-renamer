import { readdirSync } from "fs";
import Source from "./Source";

export default class FileSystemSource implements Source {
    findIn(location: string): string[] {
        return readdirSync(location)
    }
}