import Source from "./Source";

export default class InMemorySource implements Source {

   constructor(
      private filenames: string[]
   ) {}

   findIn(): string[] {
      return this.filenames
   }
}