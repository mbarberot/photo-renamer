export default interface Source {
    findIn(location: string): string[]
}