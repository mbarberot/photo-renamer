import DryRunRenameExecutor from "./executor/DryRunRenameExecutor"
import RenameExecutor from "./executor/RenameExecutor"
import RenameRequest from "./executor/RenameRequest"
import PhotoFinder from "./finder/PhotoFinder"
import Photo from "./finder/photo/Photo"

export default class PhotoRenamer {

    private IMG_PATTERN = /^IMG_(\d{4})(\d{2})(\d{2})_\d+\.(\w+)$/
    private PXL_PATTERN = /^PXL_(\d{4})(\d{2})(\d{2})_[\dMP.]+\.(\w+)$/
    private BASIC_IMG_PATTERN = /^(\d{4})(\d{2})(\d{2})_\d+\.(\w+)$/

    constructor(
        private finder: PhotoFinder = new PhotoFinder(),
        private executor: RenameExecutor = new DryRunRenameExecutor(),
    ) { }

    renamePhotos(sourceDirectory: string, destinationDirectory: string): void {

        console.log("Photo renamer")
        console.log(`Source directory : ${sourceDirectory}`)
        console.log(`Destination directory : ${destinationDirectory}`)

        const photos = this.finder.findPhotosIn(sourceDirectory)

        console.log("Found photos :", photos)

        this.executor.init()

        const renames = this.rename(photos)

        renames.forEach(request => this.executor.rename(request))


        // TODO : move information extraction into photo impl
        // TODO : handle paths 
        // TODO : recursive find (ie: handle sub path from sourceDirectory)
        // TODO : check directories
        // TODO : implements real executor
    }

    rename(photos: Photo[]): RenameRequest[] {

        // Generate requests
        const renamedFilenames: RenameRequest[] = photos
            .map(photo => {
                const filename = photo.getFilename()

                if (this.BASIC_IMG_PATTERN.test(filename)) {
                    const [_, year, month, day, extension] = this.BASIC_IMG_PATTERN.exec(filename) ?? []
                    return { from: filename, to: `${year}-${month}-${day}.${extension}` }
                }

                if (this.IMG_PATTERN.test(filename)) {
                    const [_, year, month, day, extension] = this.IMG_PATTERN.exec(filename) ?? []
                    return { from: filename, to: `${year}-${month}-${day}.${extension}` }
                }

                if (this.PXL_PATTERN.test(filename)) {
                    const [_, year, month, day, extension] = this.PXL_PATTERN.exec(filename) ?? []
                    return { from: filename, to: `${year}-${month}-${day}.${extension}` }
                }

                return { from: filename, to: filename }
            })

        // Create index to check unicity
        type FilenameMap = { [key: string]: RenameRequest[] }
        const filenameMap: FilenameMap = renamedFilenames.reduce(
            (map: FilenameMap, request: RenameRequest) => {
                const targetFilename = request.to
                map[targetFilename] = [...(map[targetFilename] || []), request]
                return map
            },
            {}
        )

        // Get unique requests
        const uniqueFilenames = Object.keys(filenameMap).filter(key => filenameMap[key].length == 1)
        const uniqueRequests: RenameRequest[] = uniqueFilenames
            .map(key => filenameMap[key])
            .reduce((array: RenameRequest[], requestsForKey: RenameRequest[]) => ([...array, ...requestsForKey]), [])

        // Fix !unique requests
        const multipleFilenames = Object.keys(filenameMap).filter(key => filenameMap[key].length > 1)
        const deduplicatedRequests = multipleFilenames
            .map((multipleFilename: string) => {
                const requests = filenameMap[multipleFilename]
                const [left, right] = multipleFilename.split('.')
                return requests.map(({from}, index) => ({from, to:  `${left} (${index + 1}).${right}`}))
            })
            .reduce((array: RenameRequest[], requests: RenameRequest[]) => ([...array, ...requests]), [])

        return [
            ...uniqueRequests,
            ...deduplicatedRequests,
        ];
    }
}

