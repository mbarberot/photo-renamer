import PhotoRenamer from "./rename/PhotoRenamer"
import DryRunRenameExecutor from "./rename/executor/DryRunRenameExecutor"
import PhotoFinder from "./rename/finder/PhotoFinder"

/*

TODO LIST

[ ] check args
[ ] print usage
[ ] check directory exists
[ ] create target dir
[ ] check disk space for copy

*/

const sourceDirectory = process.argv[2]
const destinationDirectory = process.argv[3]
const dryRun = process.env["DRY_RUN"] !== "false"

if(dryRun) {
    new PhotoRenamer(
        new PhotoFinder(),
        new DryRunRenameExecutor()
    ).renamePhotos(sourceDirectory, destinationDirectory)
}

