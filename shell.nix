with (import <nixpkgs> {});

mkShell {
    buildInputs = [
        yarn
        nodejs-18_x
        bats
    ];

    shellHook = ''
      yarn config set prefix $PWD/.yarn
      export PATH="$(yarn global bin):$PATH"
    '';
}
